package com.research.peaks.common.mapper;

import org.springframework.stereotype.Component;

import com.research.peaks.dto.UserDTO;
import com.research.peaks.model.AuthorityEntity;
import com.research.peaks.model.UserEntity;

@Component
public class UserMapper {
	public UserDTO fromEntity(UserEntity entity) {
		UserDTO dto = new UserDTO();
		dto.setEmail(entity.getEmail());
		if (entity.getAuths() != null && entity.getAuths().size() > 0) {
			AuthorityEntity auth = entity.getAuths().stream().findFirst().get();
			dto.setRole(auth.getAuthority());
		}
		dto.setUsername(entity.getUsername());
		return dto;
	}
}
