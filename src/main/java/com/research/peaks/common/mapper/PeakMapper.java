package com.research.peaks.common.mapper;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.research.peaks.dto.PeakDTO;
import com.research.peaks.model.PeakEntity;

@Component
public class PeakMapper {
	public PeakDTO fromEntity(PeakEntity entity) {
		PeakDTO dto = new PeakDTO();
		dto.setElevation(entity.getAltitude());
		dto.setId(entity.getPeakId());
		dto.setLatitude(entity.getLatitude());
		dto.setLongitude(entity.getLongitude());
		dto.setName(entity.getOfficialName());
		dto.setProvenance(entity.getProvenance());
		dto.setToBeAnnotated(entity.isToBeAnnotated());
		return dto;
	}

	public PeakEntity fromDTO(PeakDTO dto) {
		PeakEntity entity = new PeakEntity();
		entity.setPeakId(dto.getId());
		entity.setAltitude(dto.getElevation());
		entity.setLatitude(dto.getLatitude());
		entity.setLongitude(dto.getLongitude());
		entity.setOfficialName(dto.getName());
		entity.setProvenance(dto.getProvenance());
		entity.setLocalName(dto.getName());
		entity.setToBeAnnotated(dto.isToBeAnnotated());
		String[][] localized_names = dto.getLocalizedNames();
		Map<String, String> M = new HashMap<>();
		if (localized_names != null) {
			for (String[] s : localized_names) {
				M.put(s[0], s[1]);
			}
		}
		entity.setLocalizedNames(M);
		return entity;
	}
}
