package com.research.peaks.common.mapper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

import com.research.peaks.dto.CampaignDTO;
import com.research.peaks.model.CampaignEntity;

@Component
public class CampaignMapper {
	public CampaignDTO fromEntity(CampaignEntity entity) {
		CampaignDTO dto = new CampaignDTO();
		dto.setName(entity.getName());
		
		dto.setStartDate(entity.getStartDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
		dto.setEndDate(entity.getEndDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
		dto.setStatus(entity.getStatus().name());
		return dto;
	}

	public CampaignEntity fromDTO(CampaignDTO dto) {
		CampaignEntity entity = new CampaignEntity();
		entity.setName(dto.getName());
		entity.setStartDate(LocalDate.parse(dto.getStartDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy")));
		entity.setEndDate(LocalDate.parse(dto.getEndDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy")));
		return entity;
	}
	
}
