package com.research.peaks.common;

public enum CampaignStatus {
	CREATED, STARTED, CLOSED;
}
