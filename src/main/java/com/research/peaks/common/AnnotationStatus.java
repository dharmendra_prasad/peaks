package com.research.peaks.common;

public enum AnnotationStatus {
	VALID, REJECTED
}
