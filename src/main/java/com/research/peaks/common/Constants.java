package com.research.peaks.common;

public final class Constants {
	public static final String ROLE_MANAGER = "MANAGER";
	public static final String ROLE_WORKER = "WORKER";
	public static final String CAMPAIGN_NAME = "campaignName";
	public static final String CAMPAIGN_STARTED = "campaignStarted";
	public static final String MESSAGE = "message";
	public static final String MESSAGE_TYPE = "messageType";
	public static final String ALERT = "alert";
}
