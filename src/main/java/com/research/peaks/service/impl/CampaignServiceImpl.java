package com.research.peaks.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.research.peaks.common.AnnotationStatus;
import com.research.peaks.common.CampaignStatus;
import com.research.peaks.common.Constants;
import com.research.peaks.common.PeakValidity;
import com.research.peaks.common.mapper.CampaignMapper;
import com.research.peaks.common.mapper.PeakMapper;
import com.research.peaks.common.mapper.UserMapper;
import com.research.peaks.dto.AnnotationDTO;
import com.research.peaks.dto.CampaignDTO;
import com.research.peaks.dto.PeakDTO;
import com.research.peaks.dto.StatsDTO;
import com.research.peaks.dto.UserDTO;
import com.research.peaks.model.AnnotationEntity;
import com.research.peaks.model.CampaignEntity;
import com.research.peaks.model.PeakEntity;
import com.research.peaks.model.UserEntity;
import com.research.peaks.repository.CampaignRepository;
import com.research.peaks.repository.PeakRepository;
import com.research.peaks.repository.UserRepository;
import com.research.peaks.service.CampaignService;

@Service
public class CampaignServiceImpl implements CampaignService {

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PeakRepository peakRepository;

	@Autowired
	private CampaignMapper campaignMapper;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private PeakMapper peakMapper;

	@Override
	public CampaignDTO createCampaign(CampaignDTO campaign, String username) {
		CampaignEntity entity = campaignMapper.fromDTO(campaign);
		entity.setStatus(CampaignStatus.CREATED);

		UserEntity user = userRepository.findByUsername(username);
		user.addCampaign(entity);
		user = userRepository.save(user);
		Set<CampaignEntity> campaigns = user.getCampaigns();
		CampaignEntity savedCampaign = campaigns.stream().filter(c -> c.getName().equals(campaign.getName()))
				.findFirst().orElse(null);
		return campaignMapper.fromEntity(savedCampaign);
	}

	@Override
	public CampaignDTO getCampaign(String name, String username) {
		CampaignEntity campaign = campaignRepository.findByName(name);
		UserEntity user = userRepository.findByUsername(username);
		UserDTO userDTO = userMapper.fromEntity(user);

		if (Constants.ROLE_WORKER.equals(userDTO.getRole())) {
			UserEntity userEntity = campaign.getUsers().stream().filter(u -> u.getUsername().equals(username))
					.findFirst().orElse(null);
			// this is the case where worker enrolls to a new campaign
			if (userEntity == null) {
				campaign.addUser(user);
				user.addCampaign(campaign);
				userRepository.save(user);
			}
		} else if (Constants.ROLE_MANAGER.equals(userDTO.getRole())) {
			UserEntity userEntity = campaign.getUsers().stream().filter(u -> u.getUsername().equals(username))
					.findFirst().orElse(null);

			if (userEntity == null)
				return null;
		}

		return campaignMapper.fromEntity(campaign);
	}

	@Override
	public List<CampaignDTO> getAllCampaignsForUser(String username) {
		UserEntity user = userRepository.findByUsername(username);
		List<UserEntity> userList = Arrays.asList(new UserEntity[] { user });
		List<CampaignEntity> allCampaigns = campaignRepository.findAllByUsers(userList);
		System.out.println(allCampaigns);
		List<CampaignDTO> listToReturn = allCampaigns.stream().map(e -> campaignMapper.fromEntity(e))
				.collect(Collectors.toList());
		return listToReturn;
	}

	@Override
	public CampaignDTO startCampaign(String username, CampaignDTO campaign) {

		CampaignEntity campaignEntity = campaignRepository.findByName(campaign.getName());
		// UserEntity userEntity = userRepository.findByUsername(username);
		// check if user is manager and owns the campaign
		// include dates for audit
		campaignEntity.setStatus(CampaignStatus.STARTED);
		CampaignEntity updatedCampaign = campaignRepository.save(campaignEntity);
		return campaignMapper.fromEntity(updatedCampaign);
	}

	@Override
	public CampaignDTO updateCampaign(String username, CampaignDTO campaign) {
		CampaignEntity campaignEntity = campaignRepository.findByName(campaign.getName());
		// UserEntity userEntity = userRepository.findByUsername(username);
		// check if user is manager and owns the campaign
		// include dates for audit
		CampaignEntity campaignDisconnected = campaignMapper.fromDTO(campaign);

		campaignEntity.setEndDate(campaignDisconnected.getEndDate());
		campaignEntity.setStartDate(campaignDisconnected.getStartDate());

		CampaignEntity updatedCampaign = campaignRepository.save(campaignEntity);
		return campaignMapper.fromEntity(updatedCampaign);
	}

	@Override
	public CampaignDTO closeCampaign(String username, CampaignDTO campaign) {
		CampaignEntity campaignEntity = campaignRepository.findByName(campaign.getName());
		// UserEntity userEntity = userRepository.findByUsername(username);
		// check if user is manager and owns the campaign
		// include dates for audit
		campaignEntity.setStatus(CampaignStatus.CLOSED);
		CampaignEntity updatedCampaign = campaignRepository.save(campaignEntity);
		return campaignMapper.fromEntity(updatedCampaign);
	}

	@Override
	public void addPeaksToCampaign(String campaignName, File tempFile, String username, boolean annotate) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			UserEntity userEntity = userRepository.findByUsername(username);
			UserDTO user = userMapper.fromEntity(userEntity);
			CampaignEntity campaign = campaignRepository.findByName(campaignName);
			if (Constants.ROLE_MANAGER.equals(user.getRole()) && userEntity.getCampaigns().contains(campaign)) {
				PeakDTO[] peaks = mapper.readValue(tempFile, PeakDTO[].class);
				for (PeakDTO p : peaks) {
					p.setToBeAnnotated(annotate);
				}
				List<PeakEntity> peaksList = Arrays.stream(peaks).map(p -> peakMapper.fromDTO(p))
						.collect(Collectors.toList());
				peaksList.forEach(p -> p.addCampaign(campaign));
				peaksList.forEach(p -> campaign.addPeak(p));
				peakRepository.saveAll(peaksList);
				campaignRepository.save(campaign);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<PeakDTO> getPeaks(String campaignName, String username) {
		CampaignEntity campaign = campaignRepository.findByName(campaignName);
		Map<Long, PeakDTO> peakMap = campaign.getPeaks().stream()
				.collect(Collectors.toMap(PeakEntity::getPeakId, p -> peakMapper.fromEntity(p)));
		for (PeakEntity p : campaign.getPeaks()) {
			PeakDTO peakDTO = peakMap.get(p.getPeakId());
			Set<AnnotationEntity> annotations = p.getAnnotations();
			if (annotations != null && annotations.size() > 0) {
				peakDTO.setNumberOfAnnotations(annotations.size());

				List<AnnotationDTO> annotationList = annotations.stream()
						.map(a -> new AnnotationDTO(a.getId(), a.getName(), a.getElevation(), p.getPeakId(),
								a.getPeakValidity(), a.getUser().getUsername(), a.getStatus()))
						.collect(Collectors.toList());
				peakDTO.setAnnotations(annotationList);
				AnnotationEntity annotationByUser = annotations.stream()
						.filter(a -> a.getUser().getUsername().equals(username)).findFirst().orElse(null);
				if (annotationByUser != null && p.isToBeAnnotated()) {
					peakDTO.setAlreadyAnnotatedByUser(true);
				}

				List<AnnotationEntity> rejectedAnnotation = annotations.stream()
						.filter(a -> AnnotationStatus.REJECTED.equals(a.getStatus())).collect(Collectors.toList());

				if (rejectedAnnotation != null) {
					peakDTO.setNumberOfRejectedAnnotations(rejectedAnnotation.size());
				}

			} else {
				peakDTO.setNumberOfAnnotations(0);
			}

		}

		return peakMap.values().stream().collect(Collectors.toList());
	}

	@Override
	public List<CampaignDTO> getCampaignsForWorker(String username) {
		UserEntity user = userRepository.findByUsername(username);
		List<CampaignEntity> campaignEntities = campaignRepository.findByStatus(CampaignStatus.STARTED);
		List<CampaignEntity> campaingsForUser = campaignEntities.stream().filter(c -> c.getUsers().contains(user))
				.collect(Collectors.toList());
		List<CampaignDTO> campaings = campaingsForUser.stream().map(c -> campaignMapper.fromEntity(c))
				.collect(Collectors.toList());
		return campaings;
	}

	@Override
	public List<CampaignDTO> getStartedCampaignsForWorker(String username) {
		UserEntity user = userRepository.findByUsername(username);
		List<CampaignEntity> campaignEntities = campaignRepository.findByStatus(CampaignStatus.STARTED);
		List<CampaignEntity> campaingsForUser = campaignEntities.stream().filter(c -> !c.getUsers().contains(user))
				.collect(Collectors.toList());
		List<CampaignDTO> campaings = campaingsForUser.stream().map(c -> campaignMapper.fromEntity(c))
				.collect(Collectors.toList());
		return campaings;
	}

	@Override
	public StatsDTO getStats(String campaignName, String username) {
		List<PeakDTO> peaks = getPeaks(campaignName, username);
		List<PeakDTO> numPeaksStarted = peaks.stream().filter(p -> p.getAnnotations().size() == 0)
				.collect(Collectors.toList());
		List<PeakDTO> numPeakAnnotated = peaks.stream().filter(p -> p.getAnnotations().size() > 0)
				.collect(Collectors.toList());
		List<PeakDTO> numPeakWithRejectedAnnotation = peaks.stream().filter(p -> {
			List<AnnotationDTO> collect = p.getAnnotations().stream()
					.filter(a -> a.getStatus() == AnnotationStatus.REJECTED).collect(Collectors.toList());
			System.out.println(collect);
			return collect != null && collect.size() > 0;
		}).collect(Collectors.toList());

		List<PeakDTO> numOfConflicts = peaks.stream().filter(p -> {
			List<AnnotationDTO> collectInvalid = p.getAnnotations().stream()
					.filter(a -> a.getPeakValidity() == PeakValidity.INVALID).collect(Collectors.toList());
			List<AnnotationDTO> collectValid = p.getAnnotations().stream()
					.filter(a -> a.getPeakValidity() == PeakValidity.VALID).collect(Collectors.toList());
			return collectInvalid != null && collectInvalid.size() > 0 && collectValid != null
					&& collectValid.size() > 0;
		}).collect(Collectors.toList());
		
  		StatsDTO stats = new StatsDTO();
		stats.setAnnotatedPeaks(numPeakAnnotated);
		stats.setConflictedPeaks(numOfConflicts);
		stats.setPeaksWithRejectedAnnotations(numPeakWithRejectedAnnotation);
		stats.setNumOfConflicts(numOfConflicts.size());
		stats.setNumPeakWithRejectedAnnotation(numPeakWithRejectedAnnotation.size());
		stats.setNumPeakAnnotated(numPeakAnnotated.size());
		stats.setNumPeaksStarted(numPeaksStarted.size());
		
		return stats;
	}
}
