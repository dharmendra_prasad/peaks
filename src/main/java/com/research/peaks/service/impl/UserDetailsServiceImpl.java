package com.research.peaks.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.research.peaks.common.Constants;
import com.research.peaks.common.mapper.UserMapper;
import com.research.peaks.dto.UserDTO;
import com.research.peaks.exception.UserExistsException;
import com.research.peaks.model.AuthorityEntity;
import com.research.peaks.model.UserEntity;
import com.research.peaks.repository.AuthorityRepository;
import com.research.peaks.repository.UserRepository;
import com.research.peaks.service.UserService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, UserService {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private AuthorityRepository authRepo;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserMapper userMapper;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = userRepo.findByUsername(username);

		if (user != null) {
			System.out.println(user);
			System.out.println(user.getAuths());
			List<SimpleGrantedAuthority> auths = user.getAuths().stream()
					.map(a -> new SimpleGrantedAuthority(a.getAuthority())).collect(Collectors.toList());

			return new User(username, user.getPassword(), auths);
		}

		throw new UsernameNotFoundException("No user was found with name " + username);
	}

	public UserDTO register(UserDTO newUser) throws UserExistsException {
		String username = newUser.getUsername();
		UserEntity existingUser = userRepo.findByUsername(username);

		if (existingUser == null) {
			UserEntity newUserEntity = new UserEntity();
			newUserEntity.setUsername(username);
			newUserEntity.setPassword(passwordEncoder.encode(newUser.getPassword()));
			newUserEntity.setEmail(newUser.getEmail());

			AuthorityEntity authority = authRepo.findByAuthority(newUser.getRole());
			newUserEntity.addAuth(authority);
			try {
				UserEntity savedUser = userRepo.save(newUserEntity);
				if (savedUser != null) {
					return newUser;
				}
			} catch (Exception e) {
				return null;
			}
		} else {
			throw new UserExistsException();
		}
		return null;
	}

	@Override
	public UserDTO findUserByUsername(String username) {
		UserEntity userEntity = userRepo.findByUsername(username);
		return userMapper.fromEntity(userEntity);
	}

	@PostConstruct
	public void init() {
		Iterable<AuthorityEntity> allAuths = authRepo.findAll();
		List<AuthorityEntity> authorityList = new ArrayList<>();
		allAuths.iterator().forEachRemaining(a -> authorityList.add(a));
		List<String> roles = Arrays.asList(new String[] { Constants.ROLE_MANAGER, Constants.ROLE_WORKER });
		Set<AuthorityEntity> collect = authorityList.stream().filter(a -> roles.contains(a.getAuthority()))
				.collect(Collectors.toSet());
		if (collect == null || collect.size() != roles.size()) {
			collect.add(new AuthorityEntity(Constants.ROLE_MANAGER));
			collect.add(new AuthorityEntity(Constants.ROLE_WORKER));
			authRepo.saveAll(collect);
		}
	}

	@Override
	public void updateProfile(UserDTO user) {
		UserEntity userEntity = userRepo.findByUsername(user.getUsername());
		userEntity.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepo.save(userEntity);
	}
}
