package com.research.peaks.service.impl;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.research.peaks.dto.AnnotationDTO;
import com.research.peaks.dto.CampaignDTO;
import com.research.peaks.dto.PeakDTO;
import com.research.peaks.model.AnnotationEntity;
import com.research.peaks.model.PeakEntity;
import com.research.peaks.repository.AnnotationRepository;
import com.research.peaks.repository.PeakRepository;
import com.research.peaks.repository.UserRepository;
import com.research.peaks.service.AnnotationService;
import com.research.peaks.service.CampaignService;

@Service
public class AnnotationServiceImpl implements AnnotationService {

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private PeakRepository peakRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AnnotationRepository annotationRepository;

	@Override
	public void saveAnnotation(AnnotationDTO annotation, String username, String campaignName) {
		List<CampaignDTO> campaignsForWorker = campaignService.getCampaignsForWorker(username);

		CampaignDTO campaign = campaignsForWorker.stream().filter(c -> c.getName().equals(campaignName)).findFirst()
				.orElse(null);
		if (campaign != null) {
			PeakDTO peak = campaignService.getPeaks(campaignName, username).stream()
					.filter(p -> p.getId() == annotation.getPeakId()).findFirst().orElse(null);
			if (peak != null) {
				AnnotationEntity annotationEntity = new AnnotationEntity();
				annotationEntity.setUser(userRepository.findByUsername(username));
				annotationEntity.setPeakValidity(annotation.getPeakValidity());
				annotationEntity.setCreationDateTime(LocalDateTime.now());
				if (annotation.getElevation() != null) {
					annotationEntity.setElevation(annotation.getElevation());
				}
				if (annotation.getName() != null) {
					annotationEntity.setName(annotation.getName());
				}

				PeakEntity peakEntity = peakRepository.findByPeakId(peak.getId());
				annotationEntity.setPeak(peakEntity);

				String localNames = annotation.getLocalNames();
				if (localNames != null && !localNames.isEmpty()) {
					String[] split = localNames.split(":|,");
					Map<String, String> M = new HashMap<>();
					for (int i = 0; i < split.length - 1; i += 2) {
						M.put(split[0].trim(), split[1].trim());
					}
					annotationEntity.setLocalizedNames(M);
				}

				AnnotationEntity savedAnnotation = annotationRepository.save(annotationEntity);
				peakEntity.addAnnotation(savedAnnotation);
				peakRepository.save(peakEntity);
			}
		}

	}

	@Override
	public void updateAnnotationStatus(String campaignName, List<AnnotationDTO> annotationToUpdate) {
		Map<Long, AnnotationDTO> annotationMap = annotationToUpdate.stream()
				.collect(Collectors.toMap(AnnotationDTO::getAnnotationId, a -> a));
		Iterable<AnnotationEntity> annotations = annotationRepository.findAllById(annotationMap.keySet());
		annotations.forEach(a -> a.setStatus(annotationMap.get(a.getId()).getStatus()));
		annotationRepository.saveAll(annotations);
	}
}
