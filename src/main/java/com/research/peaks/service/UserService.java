package com.research.peaks.service;

import com.research.peaks.dto.UserDTO;
import com.research.peaks.exception.UserExistsException;

public interface UserService {

	UserDTO register(UserDTO newUser) throws UserExistsException;
	
	UserDTO findUserByUsername(String username);

	void updateProfile(UserDTO user);
}
