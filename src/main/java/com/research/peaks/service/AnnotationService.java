package com.research.peaks.service;

import java.util.List;

import com.research.peaks.dto.AnnotationDTO;

public interface AnnotationService {

	void saveAnnotation(AnnotationDTO annotation, String username, String campaignName);

	void updateAnnotationStatus(String campaignName, List<AnnotationDTO> annotationToUpdate);

}
