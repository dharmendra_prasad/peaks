package com.research.peaks.service;

import java.io.File;
import java.util.List;

import com.research.peaks.dto.CampaignDTO;
import com.research.peaks.dto.PeakDTO;
import com.research.peaks.dto.StatsDTO;

public interface CampaignService {
	CampaignDTO createCampaign(CampaignDTO campaign, String userId);

	CampaignDTO getCampaign(String name, String userId);

	List<CampaignDTO> getAllCampaignsForUser(String userId);

	
	CampaignDTO startCampaign(String username, CampaignDTO campaign);

	CampaignDTO updateCampaign(String username, CampaignDTO campaign);

	CampaignDTO closeCampaign(String username, CampaignDTO campaign);

	void addPeaksToCampaign(String campaignName, File tempFile, String username, boolean annotate);

	List<PeakDTO> getPeaks(String campaignName, String username);

	List<CampaignDTO> getCampaignsForWorker(String username);

	List<CampaignDTO> getStartedCampaignsForWorker(String username);

	StatsDTO getStats(String campaign, String username);
}
