package com.research.peaks.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean("passwordEncoder")
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// insecure page and resource access.
		http.authorizeRequests()
				.antMatchers("/home", "/register", "/user_security_check", "/**/images/*", "/**/js/*", "/**/css/*")
				.permitAll();

		// Ensures that any request to our application requires the user to be
		// authenticated
		http.authorizeRequests().anyRequest().authenticated();

		// login
		http.formLogin().loginPage("/home").loginProcessingUrl("/user_security_check").usernameParameter("username")
				.passwordParameter("password").defaultSuccessUrl("/dashboard", true).failureUrl("/home?error=true")
				// configure maximum allowed concurrent session
				.and().sessionManagement().maximumSessions(1);

		// logout
		http.logout().logoutUrl("/logout").logoutSuccessUrl("/home").deleteCookies("JSESSIONID")
				.invalidateHttpSession(true);

		// csrf disabled
		http.csrf().disable();

		// basic auth enabled
		http.httpBasic();
	}
}
