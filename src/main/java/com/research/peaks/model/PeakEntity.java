package com.research.peaks.model;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "peaks")
public class PeakEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	long peakId;

	double latitude;
	double longitude;
	double altitude;
	String officialName;
	String localName;
	@ElementCollection
	Map<String, String> localizedNames;
	String provenance;
	boolean toBeAnnotated;

	@OneToMany(mappedBy = "peak")
	Set<AnnotationEntity> annotations = new HashSet<>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "campaign_peaks", joinColumns = @JoinColumn(name = "peak_id"), inverseJoinColumns = @JoinColumn(name = "campaign_id"))
	private Set<CampaignEntity> campaigns = new HashSet<>();

	public void addCampaign(CampaignEntity campaign) {
		campaigns.add(campaign);
		campaign.getPeaks().add(this);
	}

	public void removeCampaign(CampaignEntity campaign) {
		campaigns.remove(campaign);
		campaign.getPeaks().remove(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public String getOfficialName() {
		return officialName;
	}

	public void setOfficialName(String officialName) {
		this.officialName = officialName;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public Map<String, String> getLocalizedNames() {
		return localizedNames;
	}

	public void setLocalizedNames(Map<String, String> localizedNames) {
		this.localizedNames = localizedNames;
	}

	public String getProvenance() {
		return provenance;
	}

	public void setProvenance(String provenance) {
		this.provenance = provenance;
	}

	public boolean isToBeAnnotated() {
		return toBeAnnotated;
	}

	public void setToBeAnnotated(boolean toBeAnnotated) {
		this.toBeAnnotated = toBeAnnotated;
	}

	public Set<CampaignEntity> getCampaigns() {
		return campaigns;
	}

	public long getPeakId() {
		return peakId;
	}

	public void setPeakId(long peakId) {
		this.peakId = peakId;
	}

	public void addAnnotation(AnnotationEntity annotation) {
		this.annotations.add(annotation);
	}

	public void removeAnnotation(AnnotationEntity annotation) {
		this.annotations.remove(annotation);
	}

	public Set<AnnotationEntity> getAnnotations() {
		return annotations;
	}

}
