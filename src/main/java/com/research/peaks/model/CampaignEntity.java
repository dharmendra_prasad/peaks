package com.research.peaks.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.research.peaks.common.CampaignStatus;

@Entity
@Table(name = "campaigns")
public class CampaignEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Column(unique = true)
	String name;

	LocalDate startDate;

	LocalDate endDate;

	@Enumerated(EnumType.STRING)
	CampaignStatus status;

	@ManyToMany(mappedBy = "campaigns")
	private Set<UserEntity> users = new HashSet<>();

	@ManyToMany(mappedBy = "campaigns")
	private Set<PeakEntity> peaks = new HashSet<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public CampaignStatus getStatus() {
		return status;
	}

	public void setStatus(CampaignStatus status) {
		this.status = status;
	}

	public Set<UserEntity> getUsers() {
		return users;
	}

	public void addUser(UserEntity user) {
		users.add(user);
	}

	public void removeUser(UserEntity user) {
		users.remove(user);
	}

	public Set<PeakEntity> getPeaks() {
		return peaks;
	}

	public void addPeak(PeakEntity peak) {
		peaks.add(peak);
	}

	public void removePeak(PeakEntity peak) {
		peaks.remove(peak);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CampaignEntity other = (CampaignEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
