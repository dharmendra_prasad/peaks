package com.research.peaks.model;

import java.time.LocalDateTime;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.research.peaks.common.AnnotationStatus;
import com.research.peaks.common.PeakValidity;

@Entity
@Table(name = "annotations")
public class AnnotationEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	LocalDateTime creationDateTime;
	double elevation;
	String name;

	@ElementCollection
	Map<String, String> localizedNames;

	@Enumerated(EnumType.STRING)
	AnnotationStatus status;

	@Enumerated(EnumType.STRING)
	PeakValidity peakValidity;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	UserEntity user;

	@ManyToOne
	@JoinColumn(name = "peak_id", nullable = false)
	PeakEntity peak;

	public LocalDateTime getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(LocalDateTime creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public double getElevation() {
		return elevation;
	}

	public void setElevation(double elevation) {
		this.elevation = elevation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getLocalizedNames() {
		return localizedNames;
	}

	public void setLocalizedNames(Map<String, String> localizedNames) {
		this.localizedNames = localizedNames;
	}

	public AnnotationStatus getStatus() {
		return status;
	}

	public void setStatus(AnnotationStatus status) {
		this.status = status;
	}

	public PeakEntity getPeak() {
		return peak;
	}

	public void setPeak(PeakEntity peak) {
		this.peak = peak;
	}

	public PeakValidity getPeakValidity() {
		return peakValidity;
	}

	public void setPeakValidity(PeakValidity peakValidity) {
		this.peakValidity = peakValidity;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnnotationEntity other = (AnnotationEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AnnotationEntity [id=" + id + ", creationDateTime=" + creationDateTime + ", elevation=" + elevation
				+ ", name=" + name + ", localizedNames=" + localizedNames + ", status=" + status + ", peakValidity="
				+ peakValidity + ", user=" + user + ", peak=" + peak + "]";
	}

}
