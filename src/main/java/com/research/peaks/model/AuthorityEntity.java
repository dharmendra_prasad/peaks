package com.research.peaks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "authorities")
public class AuthorityEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@NotNull
	@Column(unique = true)
	String authority;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public AuthorityEntity(@NotNull String authority) {
		super();
		this.authority = authority;
	}

	public AuthorityEntity() {
		super();
	}

	@Override
	public String toString() {
		return "AuthorityEntity [id=" + id + ", authority=" + authority + "]";
	}
}
