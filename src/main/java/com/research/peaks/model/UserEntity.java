package com.research.peaks.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class UserEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Column(unique = true)
	String username;

	String password;

	@Column(unique = true)
	String email;

	boolean enable = true;
	
	@OneToMany(mappedBy = "user")
	Set<AnnotationEntity> annotations = new HashSet<>();
	

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_auths", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "auth_id", referencedColumnName = "id"))
	Collection<AuthorityEntity> auths;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "user_campaign", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "campaign_id"))
	private Set<CampaignEntity> campaigns = new HashSet<>();

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public Collection<AuthorityEntity> getAuths() {
		return auths;
	}

	public void addAuth(AuthorityEntity auth) {
		if (this.auths == null) {
			auths = new ArrayList<AuthorityEntity>();
		}

		auths.add(auth);
	}

	public void addCampaign(CampaignEntity campaign) {
		campaigns.add(campaign);
		campaign.getUsers().add(this);
	}

	public void removeCampaign(CampaignEntity campaign) {
		campaigns.remove(campaign);
		campaign.getUsers().remove(this);
	}

	public Set<CampaignEntity> getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(Set<CampaignEntity> campaigns) {
		this.campaigns = campaigns;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", username=" + username + ", email=" + email + ", enable=" + enable
				+ ", auths=" + auths + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserEntity other = (UserEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
