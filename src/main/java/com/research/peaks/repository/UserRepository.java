package com.research.peaks.repository;

import org.springframework.data.repository.CrudRepository;
import com.research.peaks.model.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

	UserEntity findByUsername(String username);

	UserEntity findByEmail(String email);

}
