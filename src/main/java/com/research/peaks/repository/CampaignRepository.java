package com.research.peaks.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.research.peaks.common.CampaignStatus;
import com.research.peaks.model.CampaignEntity;
import com.research.peaks.model.UserEntity;

public interface CampaignRepository extends CrudRepository<CampaignEntity, Long> {

	List<CampaignEntity> findAllByUsers(List<UserEntity> users);
	
	CampaignEntity findByName(String name);
	
	List<CampaignEntity> findByStatus(CampaignStatus status);
}
