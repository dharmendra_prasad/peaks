package com.research.peaks.repository;

import org.springframework.data.repository.CrudRepository;

import com.research.peaks.model.PeakEntity;

public interface PeakRepository extends CrudRepository<PeakEntity, Long> {

	PeakEntity findByPeakId(Long peakId);
}
