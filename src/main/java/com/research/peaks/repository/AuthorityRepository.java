package com.research.peaks.repository;

import org.springframework.data.repository.CrudRepository;

import com.research.peaks.model.AuthorityEntity;

public interface AuthorityRepository extends CrudRepository<AuthorityEntity, Long> {

	AuthorityEntity findByAuthority(String auth);

}
