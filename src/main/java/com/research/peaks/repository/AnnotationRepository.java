package com.research.peaks.repository;

import org.springframework.data.repository.CrudRepository;

import com.research.peaks.model.AnnotationEntity;

public interface AnnotationRepository extends CrudRepository<AnnotationEntity, Long> {

}
