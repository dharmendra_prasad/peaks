package com.research.peaks.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.research.peaks.common.ValidationUtility;
import com.research.peaks.dto.UserDTO;
import com.research.peaks.exception.UserExistsException;
import com.research.peaks.service.UserService;

@Controller
public class RegisterController extends CommonController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String getRegister(Model model) {
		model.addAttribute("newUser", new UserDTO());
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String postRegister(@ModelAttribute UserDTO newUser, Model model) {

		if (!ValidationUtility.validateEmail(newUser.getEmail())) {
			addAlertToModel(model, "warning", "Please enter a valid email.");
			return "register";
		}

		try {
			UserDTO user = userService.register(newUser);
			if (user != null) {
				model.addAttribute("username", user.getUsername());
				addAlertToModel(model, "success", "Registration Sucessful");
				return "home";
			} else {
				addAlertToModel(model, "danger", "Unknown error occurred. Please contact support");
				return "register";
			}
		} catch (UserExistsException e) {
			addAlertToModel(model, "warning", "User with email already exists");
			return "register";
		}
	}
}