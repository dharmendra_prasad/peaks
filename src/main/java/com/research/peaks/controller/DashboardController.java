package com.research.peaks.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.research.peaks.common.Constants;
import com.research.peaks.dto.CampaignDTO;
import com.research.peaks.dto.UserDTO;
import com.research.peaks.service.CampaignService;
import com.research.peaks.service.UserService;

@Controller
public class DashboardController extends CommonController {
	@Autowired
	private UserService userService;

	@Autowired
	private CampaignService campaignService;

	/**
	 * same method handles dashboard for manager and worker
	 * 
	 * @param model
	 * @param principal
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = { "/dashboard", "/campaign" }, method = RequestMethod.GET)
	public String dashboard(Model model, Principal principal, HttpSession session) throws Exception {
		UserDTO user = userService.findUserByUsername(principal.getName());

		if (user != null) {
			// in case of manager this will fetch all the campaigns owned by the manager
			if (Constants.ROLE_MANAGER.equals(user.getRole())) {
				List<CampaignDTO> campaigns = campaignService.getAllCampaignsForUser(user.getUsername());
				if (campaigns != null) {
					model.addAttribute("campaignList", campaigns);
				}
			} else if (Constants.ROLE_WORKER.equals(user.getRole())) {
				List<CampaignDTO> toEnroll = campaignService.getStartedCampaignsForWorker(user.getUsername());
				List<CampaignDTO> enrolled = campaignService.getCampaignsForWorker(user.getUsername());
				if (toEnroll != null) {
					model.addAttribute("toEnroll", toEnroll);
				}
				if (enrolled != null) {
					model.addAttribute("enrolled", enrolled);
				}
			}

			model.addAttribute("campaignObj", new CampaignDTO());
			session.setAttribute("isLoggedIn", true);
			
			return "dashboard-" + user.getRole().toLowerCase();
		}
		throw new Exception("The user was null. Something went wrong for username " + principal.getName());
	}
}
