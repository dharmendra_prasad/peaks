package com.research.peaks.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.research.peaks.common.AnnotationStatus;
import com.research.peaks.common.Constants;
import com.research.peaks.common.PeakValidity;
import com.research.peaks.dto.AnnotationDTO;
import com.research.peaks.dto.Message;
import com.research.peaks.service.AnnotationService;

@Controller
public class AnnotationController extends CommonController {

	@Autowired
	private AnnotationService annotationService;

	@RequestMapping(value = "/create-annotation", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Message createAnnotation(@RequestBody MultiValueMap<String, String> annotation, Model model,
			Principal principal, HttpSession session) throws Exception {
		String campaignName = (String) session.getAttribute(Constants.CAMPAIGN_NAME);
		AnnotationDTO dto = new AnnotationDTO();
		dto.setPeakValidity(PeakValidity.VALID);
		dto.setPeakId(Long.parseLong(annotation.getFirst("peakId")));
		String name = annotation.getFirst("annotationName");
		dto.setName(name);
		dto.setElevation(Double.parseDouble(annotation.getFirst("elevation")));
		String localNames = annotation.getFirst("localNames");
		dto.setLocalNames(localNames);
		annotationService.saveAnnotation(dto, principal.getName(), campaignName);
		addAlertToModel(model, "success", "Annotation added successfully");
		return new Message("Annotation added successfully");

	}

	@RequestMapping(value = "/update-peak", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Message invalidPeak(@RequestBody MultiValueMap<String, String> annotation, Principal principal,
			HttpSession session) throws Exception {
		String campaignName = (String) session.getAttribute(Constants.CAMPAIGN_NAME);
		AnnotationDTO dto = new AnnotationDTO();
		dto.setPeakValidity(PeakValidity.INVALID);
		dto.setPeakId(Long.parseLong(annotation.getFirst("peakId")));
		annotationService.saveAnnotation(dto, principal.getName(), campaignName);
		return new Message("Peak updated successfully");
	}

	@RequestMapping(value = "/update-annotations", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public @ResponseBody Message updateAnnotations(@RequestBody MultiValueMap<String, String> annotations,
			Principal principal, HttpSession session) throws Exception {
		String campaignName = (String) session.getAttribute(Constants.CAMPAIGN_NAME);
		List<String> annotationIds = annotations.get("id");
		List<AnnotationDTO> annotationToUpdate = new ArrayList<AnnotationDTO>();
		for (int i = 0; i < annotationIds.size(); i++) {
			AnnotationDTO annotationDTO = new AnnotationDTO();
			annotationDTO.setAnnotationId(Long.parseLong(annotationIds.get(i)));
			String first = annotations.getFirst("accept_" + i);
			if (first.equalsIgnoreCase("Accept")) {
				annotationDTO.setStatus(AnnotationStatus.VALID);
			} else if (first.equalsIgnoreCase("Reject")) {
				annotationDTO.setStatus(AnnotationStatus.REJECTED);
			}
			annotationToUpdate.add(annotationDTO);
		}

		annotationService.updateAnnotationStatus(campaignName, annotationToUpdate);

		return new Message("Annotation updated successfully");
	}
}
