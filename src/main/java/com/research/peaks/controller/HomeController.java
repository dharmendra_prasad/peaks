package com.research.peaks.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.research.peaks.common.Constants;
import com.research.peaks.dto.UserDTO;
import com.research.peaks.service.UserService;

@Controller
public class HomeController extends CommonController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String homePage(@RequestParam(name = "error", required = false) boolean error, Model model) {
		if (error) {
			addAlertToModel(model, "danger", "Login Failed.");
		}
		return "home";
	}

	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public String root(Model model) {
		return "redirect:/home";
	}

	@RequestMapping(value = { "/profile" }, method = RequestMethod.GET)
	public String profile(Model model, Principal principal) {
		UserDTO user = userService.findUserByUsername(principal.getName());
		model.addAttribute("user", user);
		return "profile";
	}

	@RequestMapping(value = { "/profile-update" }, method = RequestMethod.POST)
	public String profile(Model model, Principal principal, @RequestBody MultiValueMap<String, String> userForm,
			RedirectAttributes redirectAttributes) {
		UserDTO user = userService.findUserByUsername(principal.getName());
		if (user != null) {
			String first = userForm.getFirst("password");
			user.setPassword(first);
			userService.updateProfile(user);

			redirectAttributes.addFlashAttribute(Constants.MESSAGE, "Password update successful!");
			redirectAttributes.addFlashAttribute(Constants.ALERT, true);
			redirectAttributes.addAttribute(Constants.MESSAGE_TYPE, "alert-success");
		}
		return "redirect:/home";
	}
}