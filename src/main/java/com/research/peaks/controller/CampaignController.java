package com.research.peaks.controller;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.research.peaks.common.CampaignStatus;
import com.research.peaks.common.Constants;
import com.research.peaks.common.PeakValidity;
import com.research.peaks.dto.AnnotationDTO;
import com.research.peaks.dto.CampaignDTO;
import com.research.peaks.dto.PeakDTO;
import com.research.peaks.dto.StatsDTO;
import com.research.peaks.dto.UserDTO;
import com.research.peaks.service.CampaignService;
import com.research.peaks.service.UserService;

@Controller
public class CampaignController extends CommonController {

	@Autowired
	private UserService userService;

	@Autowired
	private CampaignService campaignService;

	@RequestMapping(value = "/campaign", method = RequestMethod.POST)
	public String postCampaign(Model model, Principal principal, @ModelAttribute CampaignDTO campaign,
			HttpSession session) {
		UserDTO user = userService.findUserByUsername(principal.getName());
		CampaignDTO createdCampaign = campaignService.createCampaign(campaign, user.getUsername());
		if (createdCampaign != null) {
			model.addAttribute("campaignObj", createdCampaign);
			session.setAttribute(Constants.CAMPAIGN_NAME, createdCampaign.getName());
		}
		return "campaign-detail-" + user.getRole().toLowerCase();

	}

	@RequestMapping(value = "/campaign-detail", method = RequestMethod.GET)
	public String campaignDetail(Model model, Principal principal, @RequestParam(name = "name") String campaignName,
			HttpSession session) {
		session.setAttribute(Constants.CAMPAIGN_NAME, campaignName);

		UserDTO user = userService.findUserByUsername(principal.getName());
		CampaignDTO campaign = campaignService.getCampaign(campaignName, principal.getName());
		if (campaign != null && user != null) {
			List<PeakDTO> peaks = campaignService.getPeaks(campaignName, principal.getName());
			model.addAttribute("mapAvailable", peaks != null && peaks.size() > 0);
			model.addAttribute("campaignObj", campaign);
			if (CampaignStatus.STARTED.name().equals(campaign.getStatus())) {
				model.addAttribute(Constants.CAMPAIGN_STARTED, true);
			}
			return "campaign-detail-" + user.getRole().toLowerCase();
		} else {
			// case where the campaign exists but not for the user (only in case of MANAGER,
			// this can return null)
			addAlertToModel(model, "danger", "No Campaigns found.");
			return "dashboard-" + user.getRole().toLowerCase();
		}
	}

	@RequestMapping(value = "/update-campaign", method = RequestMethod.POST)
	public String updateCampaign(Model model, Principal principal, @ModelAttribute CampaignDTO campaign,
			@RequestParam(name = "button") String buttonValue) throws Exception {
		UserDTO user = userService.findUserByUsername(principal.getName());
		if (user != null) {
			switch (buttonValue) {
			case "Update":
				campaignService.updateCampaign(user.getUsername(), campaign);
				break;
			case "Start":
				campaignService.startCampaign(user.getUsername(), campaign);
				break;
			case "Close":
				campaignService.closeCampaign(user.getUsername(), campaign);
				break;
			}
			return "redirect:/dashboard";
		}

		throw new Exception("No user found");
	}

	@RequestMapping(value = "/campaign-map", method = RequestMethod.GET)
	public String campaignMapManager(Model model, Principal principal, HttpSession session) {
		UserDTO user = userService.findUserByUsername(principal.getName());
		String campaignName = (String) session.getAttribute(Constants.CAMPAIGN_NAME);
		CampaignDTO campaign = campaignService.getCampaign(campaignName, principal.getName());

		if (campaign != null) {
			List<PeakDTO> peaks = campaignService.getPeaks(campaignName, principal.getName());
			campaign.setPeaks(peaks);
			model.addAttribute("campaignObj", campaign);
			return "map-" + user.getRole().toLowerCase();
		} else {
			addAlertToModel(model, "warning", "No Campaign Map Found.");
			return "dashboard-" + user.getRole().toLowerCase();
		}
	}

	@RequestMapping(value = "/campaign-stats", method = RequestMethod.GET)
	public String campaignStats(Model model, Principal principal, HttpSession session) {
		UserDTO user = userService.findUserByUsername(principal.getName());
		String campaignName = (String) session.getAttribute(Constants.CAMPAIGN_NAME);
		CampaignDTO campaign = campaignService.getCampaign(campaignName, principal.getName());

		if (campaign != null) {
			StatsDTO stats = campaignService.getStats(campaignName, user.getUsername());
			model.addAttribute("stats", stats);
			return "campaign-stats";
		} else {
			addAlertToModel(model, "warning", "No Campaign Map Found.");
			return "dashboard-" + user.getRole().toLowerCase();
		}
	}

	@RequestMapping(value = "/campaign-stats-detail", method = RequestMethod.GET)
	public String peakDetail(Model model, Principal principal, HttpSession session, @RequestParam Long peakId,
			@RequestParam String report) {
		UserDTO user = userService.findUserByUsername(principal.getName());
		String campaignName = (String) session.getAttribute(Constants.CAMPAIGN_NAME);
		CampaignDTO campaign = campaignService.getCampaign(campaignName, principal.getName());

		if (campaign != null) {
			List<PeakDTO> peaks = campaignService.getPeaks(campaignName, user.getUsername());
			PeakDTO peakDTO = peaks.stream().filter(p -> p.getId() == peakId).findFirst().get();
			if (peakDTO != null) {
				model.addAttribute("annotations", peakDTO.getAnnotations());
				if ("conflict".equals(report)) {
					List<AnnotationDTO> invalid = peakDTO.getAnnotations().stream()
							.filter(p -> p.getPeakValidity() == PeakValidity.INVALID).collect(Collectors.toList());
					List<AnnotationDTO> valid = peakDTO.getAnnotations().stream()
							.filter(p -> p.getPeakValidity() == PeakValidity.VALID).collect(Collectors.toList());
					model.addAttribute("validPeakCount", valid.size());
					model.addAttribute("invalidPeakCount", invalid.size());
				}else {
					model.addAttribute("validPeakCount", 0);
					model.addAttribute("invalidPeakCount", 0);
				}
			}
			return "annotation-detail";
		} else {
			addAlertToModel(model, "warning", "No Campaign Map Found.");
			return "dashboard-" + user.getRole().toLowerCase();
		}
	}

	@RequestMapping(value = "/upload-data", method = RequestMethod.POST)
	public String handleFileUpload(@RequestParam("fileUpload") MultipartFile file, HttpSession session,
			@RequestParam(name = "button") String buttonValue, RedirectAttributes redirectAttributes,
			Principal principal) {
		String campaignName = (String) session.getAttribute(Constants.CAMPAIGN_NAME);

		if (!file.isEmpty()) {
			String prefix = String.valueOf(System.currentTimeMillis());
			File tempFile;
			try {
				boolean annotate = false;
				switch (buttonValue) {
				case "Annotate":
					annotate = true;
					break;
				case "NoAnnotate":
					annotate = false;
					break;
				}
				tempFile = File.createTempFile(prefix, file.getOriginalFilename());
				file.transferTo(tempFile);
				campaignService.addPeaksToCampaign(campaignName, tempFile, principal.getName(), annotate);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		redirectAttributes.addFlashAttribute(Constants.MESSAGE,
				"You successfully uploaded " + file.getOriginalFilename() + "!");
		redirectAttributes.addFlashAttribute(Constants.ALERT, true);
		redirectAttributes.addAttribute(Constants.MESSAGE_TYPE, "alert-success");

		return "redirect:/campaign-map";
	}
}
