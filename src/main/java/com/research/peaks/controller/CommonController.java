package com.research.peaks.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class CommonController {
	Logger logger = Logger.getLogger(CommonController.class.getName());

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public String internalServerError(Exception ex, Model model) {
		addAlertToModel(model, "danger", ex.getMessage());
		ex.printStackTrace();
		logger.log(Level.SEVERE, ex.getMessage(), ex);
		return "error";
	}

	protected void addAlertToModel(Model model, String type, String msg) {
		model.addAttribute("alert", true);
		model.addAttribute("messageType", "alert-" + type);
		model.addAttribute("message", msg);
	}
}
