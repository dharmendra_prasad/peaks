package com.research.peaks.dto;

public class Message {

	String msg;

	public Message(String msg) {
		super();
		this.msg = msg;
	}

	public Message() {
		super();
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
