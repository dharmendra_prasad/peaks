package com.research.peaks.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PeakDTO {

	private long id;
	private String provenance;
	private double elevation;
	private double longitude;
	private double latitude;
	private String name;
	private boolean toBeAnnotated;
	private int numberOfAnnotations;
	private int numberOfRejectedAnnotations;
	private String localNames;
	private boolean alreadyAnnotatedByUser;
	private List<AnnotationDTO> annotations = new ArrayList<>();

	@JsonProperty("localized_names")
	private String[][] localizedNames;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProvenance() {
		return provenance;
	}

	public void setProvenance(String provenance) {
		this.provenance = provenance;
	}

	public double getElevation() {
		return elevation;
	}

	public void setElevation(double elevation) {
		this.elevation = elevation;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[][] getLocalizedNames() {
		return localizedNames;
	}

	public void setLocalizedNames(String[][] localizedNames) {
		this.localizedNames = localizedNames;
	}

	public boolean isToBeAnnotated() {
		return toBeAnnotated;
	}

	public void setToBeAnnotated(boolean toBeAnnotated) {
		this.toBeAnnotated = toBeAnnotated;
	}

	public int getNumberOfAnnotations() {
		return numberOfAnnotations;
	}

	public void setNumberOfAnnotations(int numberOfAnnotations) {
		this.numberOfAnnotations = numberOfAnnotations;
	}

	public int getNumberOfRejectedAnnotations() {
		return numberOfRejectedAnnotations;
	}

	public void setNumberOfRejectedAnnotations(int numberOfRejectedAnnotations) {
		this.numberOfRejectedAnnotations = numberOfRejectedAnnotations;
	}

	public String getLocalNames() {
		return localNames;
	}

	public void setLocalNames(String localNames) {
		this.localNames = localNames;
	}

	public boolean isAlreadyAnnotatedByUser() {
		return alreadyAnnotatedByUser;
	}

	public void setAlreadyAnnotatedByUser(boolean alreadyAnnotatedByUser) {
		this.alreadyAnnotatedByUser = alreadyAnnotatedByUser;
	}

	public List<AnnotationDTO> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<AnnotationDTO> annotations) {
		this.annotations = annotations;
	}

	@Override
	public String toString() {
		return "PeakDTO [id=" + id + ", provenance=" + provenance + ", elevation=" + elevation + ", longitude="
				+ longitude + ", latitude=" + latitude + ", name=" + name + ", localizedNames=" + localizedNames + "]";
	}

}
