package com.research.peaks.dto;

import java.util.List;

public class StatsDTO {

	int numPeaksStarted;
	int numPeakAnnotated;
	int numPeakWithRejectedAnnotation;
	int numOfConflicts;
	List<PeakDTO> annotatedPeaks;
	List<PeakDTO> peaksWithRejectedAnnotations;
	List<PeakDTO> conflictedPeaks;
	int conflitValid;
	int conflictInvalid;

	public int getNumPeaksStarted() {
		return numPeaksStarted;
	}

	public void setNumPeaksStarted(int numPeaksStarted) {
		this.numPeaksStarted = numPeaksStarted;
	}

	public int getNumPeakAnnotated() {
		return numPeakAnnotated;
	}

	public void setNumPeakAnnotated(int numPeakAnnotated) {
		this.numPeakAnnotated = numPeakAnnotated;
	}

	public int getNumPeakWithRejectedAnnotation() {
		return numPeakWithRejectedAnnotation;
	}

	public void setNumPeakWithRejectedAnnotation(int numPeakWithRejectedAnnotation) {
		this.numPeakWithRejectedAnnotation = numPeakWithRejectedAnnotation;
	}

	public int getNumOfConflicts() {
		return numOfConflicts;
	}

	public void setNumOfConflicts(int numOfConflicts) {
		this.numOfConflicts = numOfConflicts;
	}

	public List<PeakDTO> getAnnotatedPeaks() {
		return annotatedPeaks;
	}

	public void setAnnotatedPeaks(List<PeakDTO> annotatedPeaks) {
		this.annotatedPeaks = annotatedPeaks;
	}

	public List<PeakDTO> getPeaksWithRejectedAnnotations() {
		return peaksWithRejectedAnnotations;
	}

	public void setPeaksWithRejectedAnnotations(List<PeakDTO> peaksWithRejectedAnnotations) {
		this.peaksWithRejectedAnnotations = peaksWithRejectedAnnotations;
	}

	public List<PeakDTO> getConflictedPeaks() {
		return conflictedPeaks;
	}

	public void setConflictedPeaks(List<PeakDTO> conflictedPeaks) {
		this.conflictedPeaks = conflictedPeaks;
	}

	public int getConflitValid() {
		return conflitValid;
	}

	public void setConflitValid(int conflitValid) {
		this.conflitValid = conflitValid;
	}

	public int getConflictInvalid() {
		return conflictInvalid;
	}

	public void setConflictInvalid(int conflictInvalid) {
		this.conflictInvalid = conflictInvalid;
	}

}