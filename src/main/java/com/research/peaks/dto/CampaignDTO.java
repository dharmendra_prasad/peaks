package com.research.peaks.dto;

import java.util.List;

public class CampaignDTO {
	private String name;
	private String startDate;
	private String endDate;
	private String status;
	private List<PeakDTO> peaks;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<PeakDTO> getPeaks() {
		return peaks;
	}

	public void setPeaks(List<PeakDTO> peaks) {
		this.peaks = peaks;
	}

	@Override
	public String toString() {
		return "CampaignDTO [name=" + name + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}
