package com.research.peaks.dto;

import com.research.peaks.common.AnnotationStatus;
import com.research.peaks.common.PeakValidity;

public class AnnotationDTO {

	private String localNames;
	private String name;
	private Double elevation;
	private Long peakId;
	private PeakValidity peakValidity;
	private String createdBy;
	private long annotationId;

	private AnnotationStatus status;

	public String getLocalNames() {
		return localNames;
	}

	public void setLocalNames(String localNames) {
		this.localNames = localNames;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getElevation() {
		return elevation;
	}

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	public Long getPeakId() {
		return peakId;
	}

	public void setPeakId(Long peakId) {
		this.peakId = peakId;
	}

	public PeakValidity getPeakValidity() {
		return peakValidity;
	}

	public void setPeakValidity(PeakValidity peakValidity) {
		this.peakValidity = peakValidity;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getAnnotationId() {
		return annotationId;
	}

	public void setAnnotationId(long annotationId) {
		this.annotationId = annotationId;
	}

	public AnnotationStatus getStatus() {
		return status;
	}

	public void setStatus(AnnotationStatus status) {
		this.status = status;
	}

	public AnnotationDTO(long id, String name, Double elevation, Long peakId, PeakValidity peakValidity,
			String createdBy, AnnotationStatus status) {
		super();
		this.annotationId = id;
		this.name = name;
		this.elevation = elevation;
		this.peakId = peakId;
		this.peakValidity = peakValidity;
		this.createdBy = createdBy;
		this.status = status;
	}

	public AnnotationDTO() {
		super();
	}

	@Override
	public String toString() {
		return "AnnotationDTO [localNames=" + localNames + ", name=" + name + ", elevation=" + elevation + ", peakId="
				+ peakId + ", peakValidity=" + peakValidity + ", createdBy=" + createdBy + ", annotationId="
				+ annotationId + ", status=" + status + "]";
	}

}
