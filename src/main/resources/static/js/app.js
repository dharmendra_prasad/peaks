$(document)
		.ready(
				function() {
					var greenIcon = new L.Icon({
						iconUrl : 'images/markers/green.gif',
						shadowUrl : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
						iconSize : [ 15, 15 ],
						iconAnchor : [ 12, 41 ],
						popupAnchor : [ 1, -34 ],
						shadowSize : [ 25, 25 ]
					});
					var yellowIcon = new L.Icon({
						iconUrl : 'images/markers/yellow.gif',
						shadowUrl : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
						iconSize : [ 15, 15 ],
						iconAnchor : [ 12, 41 ],
						popupAnchor : [ 1, -34 ],
						shadowSize : [ 25, 25 ]
					});
					var redIcon = new L.Icon({
						iconUrl : 'images/markers/red.gif',
						shadowUrl : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
						iconSize : [ 15, 15 ],
						iconAnchor : [ 12, 41 ],
						popupAnchor : [ 1, -34 ],
						shadowSize : [ 25, 25 ]
					});
					var orangeIcon = new L.Icon({
						iconUrl : 'images/markers/orange.gif',
						shadowUrl : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
						iconSize : [ 15, 15 ],
						iconAnchor : [ 12, 41 ],
						popupAnchor : [ 1, -34 ],
						shadowSize : [ 25, 25 ]
					});
					
					var peakArray = campaign.peaks;
					var markers = [];
					var centerIndex = Math.floor((Math.random() * peakArray.length) + 1);
					var latitude = 51.505;
					var longitude = -0.09;
					if (peakArray != undefined && peakArray[centerIndex] != undefined) {
						latitude = peakArray[centerIndex].latitude;
						longitude = peakArray[centerIndex].longitude;
					}

					// initialize map
					var mymap = L.map('mapid').setView([ latitude, longitude ], 10);

					L
							.tileLayer(
									'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
									{
										attribution : 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
										maxZoom : 18,
										id : 'mapbox.streets',
										accessToken : 'pk.eyJ1Ijoib2ZmaWNpYWxkaGFyYW0iLCJhIjoiY2ppejd0enA1MDN5dzNscGp5eHJ0cnhkMyJ9.eIo_3lnrP5uaSyTqqjk54w'
									}).addTo(mymap);

					// function to find a peak given the id
					var findPeakInArray = function(arr, peakId) {
						for (var i = 0; i < arr.length; i++) {
							if (arr[i].id === peakId) {
								return arr[i];
							}
						}
					}

					// handle click on peaks
					var groupClick = function(event) {

						var metas = document.getElementsByTagName('meta');
						var role = document.querySelector("meta[name='role']").getAttribute("content");
						var peakId = event.layer.peakId;
						var peak = findPeakInArray(peakArray, peakId);
						if (role === 'worker') {
							$("#updatePeakFormDiv").empty();
							$("#validPeakDiv").empty();

							if (peak.toBeAnnotated && !peak.alreadyAnnotatedByUser) {
								$("#validPeakDiv").append('<div class="radioClass"><input type="radio" name="validity" id="peakValid"><label>Valid</label></div>');
								$("#validPeakDiv").append('<div class="radioClass"><input type="radio" name="validity" id="peakInValid"><label>Invalid</label></div>');
								$("#peakValid")
										.click(
												function() {
													$("#updatePeakFormDiv").empty();
													if (this.checked) {
														$("#updatePeakFormDiv").append('<form id="updatePeakForm">');
														$("#updatePeakFormDiv form").append(
																'<div class="form-group"><label for="peakId">Peak ID:</label> <input type="text" readOnly class="form-control" id="peakId" name="peakId" value="'
																		+ peakId + '"></div>');
														$("#updatePeakFormDiv form")
																.append(
																		'<div class="form-group"> <label for="elevation">Elevation:</label> <input type="text" class="form-control" id="elevation" name="elevation"></div>');
														$("#updatePeakFormDiv form").append(
																'<div class="form-group"> <label for="name">Name:</label> <input type="text" class="form-control" id="name" name="annotationName"> </div>');
														$("#updatePeakFormDiv form")
																.append(
																		'<div class="form-group"> <label for="localNames">Localized Names:</label> <input type="text" placeholder="en:Mt.Everest, ch:Phung" class="form-control" id="localNames" name="localNames"> </div>');
														$("#updatePeakFormDiv form")
																.append(
																		'<div class="row"> <div class="col-sm-10"> <button  type="button" id="annotationButton" class="btn btn-success">Create Annotation</button> </div> </div>');

														$("#annotationButton").click(function() {
															console.log($("#updatePeakForm").serialize());
															$.ajax({
																url : '/create-annotation',
																type : 'post',
																dataType : 'json',
																data : $("#updatePeakForm").serialize(),
																success : function(data) {
																	$("#alertMsg").text("Annotation created successfully");
																	$("#dialog").show();
																	console.log("Annotation created successfully");
																	setTimeout(function(){ location.reload(); }, 3000);
																},
																error: function(err){
																	console.log(err+" response");
																}

															});
														});
													} else {
														$("#updatePeakFormDiv").empty();
													}
												});
								$("#peakInValid").click(
										function() {
											$("#updatePeakFormDiv").empty();
											if (this.checked) {
												$("#updatePeakFormDiv").append('<form id="markInvalidForm">');
												$("#updatePeakFormDiv form").append(
														'<div class="form-group"><label for="peakId">Peak ID:</label> <input type="text" readOnly class="form-control" id="peakId" name="peakId" value="'
																+ peakId + '"></div>');
												$("#updatePeakFormDiv").append(
														'<div class="row"> <div class="col-sm-10"> <button type="button" id="invalidButton" class="btn btn-success">Mark Peak as Invalid</button> </div> </div>');

												$("#invalidButton").click(function() {
													$.ajax({
														url : '/update-peak',
														type : 'post',
														dataType : 'json',
														data : $("#markInvalidForm").serialize(),
														success : function(data) {
															$("#alertMsg").text("Peak marked invalid");
															$("#dialog").show();
															console.log("Peak marked invalid");
															setTimeout(function(){ location.reload(); }, 3000);
														}

													});
												});
											}
										});
							}
						} else if (role === 'manager') {
							console.log("manager clicked the peak: " + event.layer.peakId);
							$("#inspectAnnotationDiv").empty();
							if (peak.numberOfAnnotations != undefined && peak.numberOfAnnotations > 0) {
								$("#inspectAnnotationDiv").append('<form id="annotationForm">');
								$("#inspectAnnotationDiv form")
										.append(
												'<table class="table table-striped" id="inspectionTable"><thead><th>PeakID</th><th>AnnotationID</th><th>Name</th><th>Elevation</th><th>Peak Validity</th><th>Worker</th><th>Accept</th><th>Reject</th></thead>');
								$("#inspectionTable").append('<tbody>')
								var htmlStr = '';
								for (var i = 0; i < peak.annotations.length; i++) {
									console.log("i ------> " + i)
									htmlStr += '<tr><td><input class="form-control" type="text" readonly name="peakId" value="' + peakId
											+ '"></td><td><input class="form-control" type="text" readonly name="id" value="' + peak.annotations[i].annotationId
											+ '"></td><td><input class="form-control" type="text" readonly name="name" value="' + peak.annotations[i].name
											+ '"></td><td><input class="form-control" type="text" readonly name="elevation" value="' + peak.annotations[i].elevation
											+ '"></td><td><input class="form-control" type="text" readonly name="peakValidity" value="' + peak.annotations[i].peakValidity
											+ '"></td><td><input class="form-control" type="text" readonly name="createdBy" value="' + peak.annotations[i].createdBy
											+ '"></td><td><input class="form-control" type="radio" value="accept" name="accept_' + i
											+ '"></td><td><input class="form-control" type="radio" value="reject"  name="accept_' + i + '"></td></tr>';

								}
								$("#inspectionTable tbody").append(htmlStr);
								$("#inspectAnnotationDiv form").append('<div class="row"> <div class="col-sm-10"> <button type="button" id="inspectBtn" class="btn btn-success">Update</button> </div> </div>');
								$("#inspectBtn").click(function() {
									
									$.ajax({
										url : '/update-annotations',
										type : 'post',
										data : $("#annotationForm").serialize(),
										dataType:'json',
										success : function(data) {
											$("#alertMsg").text("Annotations updated");
											$("#dialog").show();
											console.log("Annotations updated");
											setTimeout(function(){ location.reload(); }, 3000);
										},
										error: function(err){
											console.log(err+" response");
										}

									});
								});

							}
						}
					}

					// define a peak feature group
					var peakFeatureGroup = L.featureGroup().addTo(mymap).on("click", groupClick);

					// add all the markers to the map
					$.each(peakArray, function(index, value) {
						var icon = yellowIcon;

						if (value.toBeAnnotated) {
							if (value.numberOfRejectedAnnotations != undefined && value.numberOfRejectedAnnotations > 0) {
								icon = redIcon;
							} else if (value.numberOfAnnotations != undefined && value.numberOfAnnotations == 0) {
								icon = yellowIcon;
							} else if (value.numberOfAnnotations != undefined && value.numberOfAnnotations > 0) {
								icon = orangeIcon;
							}

						} else {
							icon = greenIcon;
						}

						var marker = L.marker([ value.latitude, value.longitude ], {
							icon : icon
						}).addTo(peakFeatureGroup);

						marker.bindPopup("<b>Name:" + value.name + "</b><br><span>Provenance: " + value.provenance + "</span><br><span>Latitude: " + value.latitude + "</span><br><span>Longitude: "
								+ value.longitude + "</span><br><span>Altitude: " + value.elevation + "</span>");
						marker.peakId = value.id;
						markers[value.id] = marker;
					});
				});
